
#include <stdlib.h>
#include <iostream>
#include "dynlibs/Module.h"
#include "dynlibs/ModuleDescription.h"
#include "ModTest.h"

// belong in ModTest.cxx
ModTest      *ModTest::s_instance = 0;
const Module *ModTest::s_module   = 0;

static void call_function(const Module *mod)
{
    if(mod != 0) {
        std::cerr << "Module found: " << mod->name() << std::endl;
    } else {
        exit(EXIT_FAILURE);
    }

    typedef void (*fun)();
    
    fun sym = mod->function<fun>("create_me");
    if(sym != 0) {
        std::cerr << "Symbol found: create_me" << std::endl;
    }

    sym();
}

static void build_descriptors()
{

    // we build our own module descriptor by hand
    // normally this would come from the database

    {
        std::vector<std::string> libs;
        std::vector<std::string> mods;
        libs.push_back("modtest1");
        Module::add("Test", libs, mods);
    }
    

    {
        std::vector<std::string> libs;
        std::vector<std::string> mods;
        libs.push_back("modtest2");
        Module::add("Test1", libs, mods);
    }

}

int main(int argc, char *argv[])
{

    build_descriptors();

    const Module *mod = Module::get("Test");
    call_function(mod);
    mod->release();

    mod = Module::get("Test1");
    call_function(mod);

    ModTest *test = ModTest::instance();
    test->do_it();

    mod->release();

    return (EXIT_SUCCESS);
}
