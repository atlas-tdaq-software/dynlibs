
#include <iostream>
#include "ModTest.h"

extern "C"
void create_me()
{
    std::cout << "Modtest1 : create me called" << std::endl;
}

class ModTest1 : public ModTest {
public:
    ModTest1() {};
    virtual void do_it() {
        std::cout << "ModTest1 object" <<  std::endl;
    }
};

static ModTest1 s_test;
