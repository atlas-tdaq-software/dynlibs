
#include <iostream>
#include "ModTest.h"

extern "C"
void create_me()
{
    std::cout << "Modtest2 : create me called" << std::endl;
}

class ModTest2 : public ModTest {
public:
    ModTest2() {};
    virtual void do_it() {
        std::cout << "ModTest2 object" <<  std::endl;
    }
};

static ModTest2 s_test;
