// this is -*- c++ -*-
#ifndef DYNLIBS_MODTEST_H_
#define DYNLIBS_MODTEST_H_

#include "dynlibs/Module.h"

class ModTest {
public:
    ModTest() {
        s_instance = this;
    }

    virtual void do_it() = 0;

    static ModTest *instance() {
        if(s_instance == 0) {
            s_module = Module::get("ModTest");
        }
        return s_instance;
    }

private:
    static ModTest       *s_instance;
    static const Module  *s_module;
};

#endif // DYNLIBS_MODTEST_H_
