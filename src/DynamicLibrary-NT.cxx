
#include <windows.h>
#include "dynlibs/DynamicLibrary.h"

DynamicLibrary::DynamicLibrary(const std::string& name)
    : m_handle(0)
{
    if((m_handle = LoadLibrary(name.c_str())) == 0) {
	throw dynlibs::DynamicLibraryNotFound(ERS_HERE,name);
    }
}

DynamicLibrary::~DynamicLibrary()
{
}

void *DynamicLibrary::symbol(const std::string& name)
{
    if(m_handle) {
        return GetProcAddress((HINSTANCE )m_handle, name.c_str());
    } else {
        return 0;
    }
}

void DynamicLibrary::release()
{
    if(m_handle) {
        FreeLibrary((HINSTANCE)m_handle);
    }
}

std::string DynamicLibrary::prefix()
{
    return "";
}

std::string DynamicLibrary::suffix()
{
    return ".dll";
}
