//
// $Id$
//

#include <assert.h>

#include "dynlibs/DynamicLibrary.h"
#include "dynlibs/Module.h"
#include "dynlibs/ModuleDescription.h"

#include <algorithm>


namespace daq { 
    namespace dynlibs {

        std::list<Module*>             Module::s_modules;
        std::list<ModuleDescription *> Module::s_descr;

        namespace {

            struct deleter {
                template<class T>
                void operator()(T *ptr) { delete ptr; }
            };

        }

        struct Cleanup {
            ~Cleanup() 
            {
                using namespace std;
	    
                for_each(Module::s_descr.begin(), Module::s_descr.end(), deleter());
                Module::s_descr.clear();
	    
                for(list<Module*>::iterator it = Module::s_modules.begin(); 
                    it != Module::s_modules.end(); 
                    ++it) {
                    delete (*it);
                }
                Module::s_modules.clear();
            }
        };
    
        Cleanup s_cleanup;
    
        const Module *Module::get(const std::string& name, bool load)
        {
            return get_unlocked(name, load);
        }

        Module *Module::get_unlocked(const std::string& name, bool /* load */ )
        {
            for(std::list<Module*>::iterator it = s_modules.begin();
                it != s_modules.end(); ++it) {
                if((*it)->name() == name) {
                    return (*it)->ref();
                }
            }

            Module *mod = new Module(name);
            s_modules.push_back(mod);
            return mod;
        }

        void *Module::symbol(const std::string& name) const
        {
            for(std::list<DynamicLibrary*>::const_iterator it = m_libs.begin(); 
                it != m_libs.end(); 
                ++it) {
                if(void *sym = (*it)->symbol(name)) {
                    return sym;
                }
            }
            return 0;
        }

        Module::Module(const std::string& name)
            : m_name(name),
              m_ref(1)
        {
            if(ModuleDescription *descr = get_description(name)) {
                load_modules(descr);
                load_libs(descr);
            } else {
                throw ModuleNotFound(ERS_HERE);
            }
        }

        Module::~Module()
        {
            free_libraries();
            free_modules();
        }

        void Module::release() const
        {
            if(--m_ref == 0) {
                delete this;
            }
        }

        Module *Module::ref()
        {
            m_ref++;
            return this;
        }

        const std::string& Module::name() const
        {
            return m_name;
        }


        void Module::add(const std::string& name, 
                         const std::vector<std::string>& libs,
                         const std::vector<std::string>& modules)
        {
            s_descr.push_back(new ModuleDescription(name, libs, modules));
        }


        ModuleDescription *Module::get_description(const std::string& name) const 
        {
            for(std::list<ModuleDescription*>::iterator it = s_descr.begin();
                it != s_descr.end();
                ++it) {
                if((*it)->name() == name) {
                    return *it;
                }
            }
            return 0;
        }

        void Module::load_modules(ModuleDescription *descr) 
        {
            for(ModuleDescription::const_iterator it = descr->begin_modules();
                it != descr->end_modules();
                ++it) {
                if(Module *mod = get_unlocked((*it),true)) {
                    m_modules.push_back(mod);
                } else {
                    free_modules();
                    throw ModuleNotFound(ERS_HERE);
                }
            }
        }

        void Module::load_libs(ModuleDescription *descr)
        {
            std::string prefix = DynamicLibrary::prefix();

            for(ModuleDescription::const_iterator it = descr->begin_libs();
                it != descr->end_libs(); 
                ++it) {
                try {
                    m_libs.push_back(new DynamicLibrary(prefix + *it));
                } catch (DynamicLibraryNotFound& ex) {
                    free_libraries();
                    free_modules();
                    throw;
                }
            }
        }


        void Module::free_libraries()
        {
            for(std::list<DynamicLibrary*>::iterator it = m_libs.begin();
                it != m_libs.end();
                ++it) {
                (*it)->release();
                delete *it;
            }
        }

        void Module::free_modules()
        {
            for(std::list<Module*>::iterator it = m_modules.begin();
                it != m_modules.end();
                ++it) {
                (*it)->release();
            }
        }
    }
}
