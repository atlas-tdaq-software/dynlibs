
#include <dlfcn.h>
#include <string>

#include "dynlibs/DynamicLibrary.h"

namespace daq { 
    namespace dynlibs {

        DynamicLibrary::DynamicLibrary(const std::string& name)
            : m_handle(0)
        {
            // our convention: name + .so
            // no version are supported
            std::string s = name+ ".so";

            if((m_handle = dlopen(s.c_str(), RTLD_NOW | RTLD_GLOBAL)) == 0) {
                throw DynamicLibraryNotFound(ERS_HERE,name, dlerror());
            }
        }

        DynamicLibrary::~DynamicLibrary()
        {
        }

        void *DynamicLibrary::symbol(const std::string& name)
        {
            if(m_handle) {
                return dlsym(m_handle, name.c_str());
            } else {
                return 0;
            }
        }

        void DynamicLibrary::release()
        {
            if(m_handle) {
                dlclose(m_handle);
            }
        }

        std::string DynamicLibrary::prefix() 
        {
            return "lib";
        }

        std::string DynamicLibrary::suffix() 
        {
            return ".so";
        }

    }
}
