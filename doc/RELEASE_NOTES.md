
# dynlibs - Load shared libraries

This package is deprecated from tdaq-09-00-00 onwards.

Please use plain `dlopen()` or [boost::dll](https://www.boost.org/doc/libs/1_72_0/doc/html/boost_dll.html)
instead. Note that unlike in this package, the `boost::dll::shared_library` object has to stay in 
scope as long as the shared library is used !

## Example of possible replacement

```c++
#include <boost/dll.hpp>

double example()
{
   boost::dll::shared_library lib("libmyplugin.so",
                                  boost::dll::load_mode::type::search_system_folders |
                                  boost::dll::load_mode::type::rtld_now);
   // Get pointer to function with given signature
   auto f = lib.get<double(double, double)>("my_function")
   return f(10.2, 3.141);
}
```
