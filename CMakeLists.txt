
tdaq_package()

tdaq_add_library(dynlibs src/Module.cxx src/DynamicLibrary-unix.cxx 
  INCLUDE_DIRECTORIES PRIVATE Boost
  OPTIONS PRIVATE -Wno-deprecated-declarations
  LINK_LIBRARIES tdaq-common::ers)

tdaq_add_executable(test_modules NOINSTALL test/test_modules.cxx LINK_LIBRARIES Boost dynlibs OPTIONS PRIVATE -Wno-deprecated-declarations)
tdaq_add_library(modtest1 NOINSTALL MODULE test/modtest1.cxx INCLUDE_DIRECTORIES PRIVATE Boost OPTIONS PRIVATE -Wno-deprecated-declarations)
tdaq_add_library(modtest2 NOINSTALL MODULE test/modtest2.cxx INCLUDE_DIRECTORIES PRIVATE Boost OPTIONS PRIVATE -Wno-deprecated-declarations)
