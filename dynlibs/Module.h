// this is -*- c++ -*-
#ifndef DYNLIBS_MODULE_H_ 
#define DYNLIBS_MODULE_H_

//
// $Id$
//

#include <string>
#include <vector>
#include <list>

#include "ers/Issue.h"
#include "dynlibs/DynamicLibrary.h"

namespace daq {

    // Exception thrown if module is not found. 
    ERS_DECLARE_ISSUE(dynlibs, ModuleNotFound, , )
    
    namespace dynlibs {
	class ModuleDescription;

	class Cleanup;

	//
	// A high-level interface to a loadable module.
	// A module is identified by a single label. The configuration file
	// is used to map this label to multiple dynamic libraries which are
	// loaded in the right order.
	//

	class [[deprecated("Please use boost:dll instead")]] Module {
	public:
	    // Get handle to module.
	    // 
	    // 'load' specifies if the module should be
	    // loaded if it's not yet in memory
	    static const Module *get(const std::string& name, bool load = true);

	    // Find symbol in module. 
	    //
	    // Searches through all libraries belonging
	    // to this module.
	    void *symbol(const std::string& name) const;

            template<class F>
            F function(const std::string& name) const 
            {
                for(std::list<DynamicLibrary*>::const_iterator it = m_libs.begin(); 
                    it != m_libs.end(); 
                    ++it) {
                    if(F sym = (*it)->function<F>(name)) {
                        return sym;
                    }
                }
                return 0;                
            }

	    // Add mapping from module name to list of libraries and modules
	    static void add(const std::string& name, 
			    const std::vector<std::string>& libs,
			    const std::vector<std::string>& modules);

	private:
	    Module(const std::string& name);

	protected:
	    ~Module();

	public:

	    // get new reference to this Module
	    Module *ref();

	    // release module
	    void release() const;

	    // return name of module
	    const std::string& name() const;

	private:
	    ModuleDescription *get_description(const std::string& name) const;
	    void load_modules(ModuleDescription *descr);
	    void load_libs(ModuleDescription *descr);

	    void free_libraries();
	    void free_modules();

	    // same as get(), but without mutex lock
	    static Module *get_unlocked(const std::string& name, bool load);
    
	private:
	    std::string                 m_name;
	    std::list<DynamicLibrary*>  m_libs;
	    std::list<Module*>          m_modules;
	    mutable unsigned int        m_ref;

	private:
	    static std::list<ModuleDescription*> s_descr;
	    static std::list<Module*>            s_modules;

	    friend class daq::dynlibs::Cleanup;
	};
    }
}

using daq::dynlibs::Module;
namespace dynlibs { using daq::dynlibs::ModuleNotFound; }

#endif // DYNLIBS_MODULE_H_
