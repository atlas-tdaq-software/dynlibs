// this is -*- c++ -*-
#ifndef DYNLIBS_MODULEDESCRIPTION_H_
#define DYNLIBS_MODULEDESCRIPTION_H_

#include <string>
#include <vector>

namespace daq { namespace dynlibs {

class ModuleDescription {
public:
    ModuleDescription(const std::string& name,
                      const std::vector<std::string>& libs,
                      const std::vector<std::string>& modules)
        : m_name(name),
          m_libs(libs),
          m_modules(modules)
    {}
    ~ModuleDescription() {}
public:
    std::string name() const { return m_name; }

    typedef std::vector<std::string>::const_reverse_iterator const_iterator;

    const_iterator begin_libs()    const { return m_libs.rbegin(); }
    const_iterator end_libs()      const { return m_libs.rend(); }
    const_iterator begin_modules() const { return m_modules.rbegin(); }
    const_iterator end_modules()   const { return m_modules.rend(); }

private:
    std::string              m_name;
    std::vector<std::string> m_libs;
    std::vector<std::string> m_modules;
};

}}

using daq::dynlibs::ModuleDescription;

#endif // DYNLIBS_MODULEDESCRIPTION_H_
