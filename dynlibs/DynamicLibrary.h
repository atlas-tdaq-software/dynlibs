// this is -*- C++ -*- 
#ifndef DYNLIBS_DYNAMICLIBRARY_H_
#define DYNLIBS_DYNAMICLIBRARY_H_

#include <string>

#include "ers/Issue.h"
#include <dlfcn.h>

namespace daq {

/**
 * Exception thrown if dynamic library is not found. The 
 * 'name' attribute is the name of the library we tried to load.
 */

ERS_DECLARE_ISSUE(dynlibs, DynamicLibraryNotFound, 
		  "Dynamic library "<< name << " not found. Reason: " << reason,
		  ((std::string)name)
                  ((std::string)reason)
		  )

}

namespace daq { namespace dynlibs {

/**
 * Interface to run-time loader. Used to load shared libraries and
 * object files dynamically at run-time into the executing process.
 *
 * Under Unix this will try to load a file called 'name'+'.so'.
 */
class  [[deprecated("Please use boost:dll instead")]] DynamicLibrary {
public:
    /// Constructor, throws DynLibNotFound().
    DynamicLibrary(const std::string& name);
    ~DynamicLibrary();
public:
    /// Lookup symbol in shared library
    void *symbol(const std::string& name);

    /// Lookup function of type F in shared library.
    /// This avoids the usual warning about casting from void* to function
    /// pointer.
    template<class F>
    F function(const std::string& name) {
        union {
            void *from;
            F    to;
        } u;

        u.from = dlsym(m_handle, name.c_str());
        return u.to;

    }

    /** Release the shared library. By default shared object files
     * stay in memory until the program exits.
     */
    void release();

    static std::string prefix();
    static std::string suffix();

private:
    void *m_handle;
};

}}

// for backward compatibility

using daq::dynlibs::DynamicLibrary;
namespace dynlibs { using daq::dynlibs::DynamicLibraryNotFound; }


#endif // DYNLIBS_DYNAMICLIBRARY_H_
